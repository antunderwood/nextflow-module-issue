params as defined in `nextflow.config` are 
```
params {
    modules {
        foo='bar' 
        baz='qux'
    }
}
```
When `other.config` is included
```
params {
    modules {
        foo='boo'
    }
} 
```
the key baz is deleted

Nextflow version 21.10.6
```
NXF_VER=21.10.6 nextflow run main.nf -profile other
N E X T F L O W  ~  version 21.10.6
Launching `main.nf` [jovial_jones] - revision: 39366da384
[foo:boo, baz:qux]
```

Nextflow version 22.04.0
```
NXF_VER=22.04.0 nextflow run main.nf -profile other
N E X T F L O W  ~  version 22.04.0
Launching `main.nf` [furious_swanson] DSL2 - revision: 39366da384
[foo:boo]
```